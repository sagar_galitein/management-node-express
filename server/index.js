const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "admin@123",
  database: "studentDetails",
});

// login
app.get(`/studentDetails/login`, (req, res) => {
  const { email, password } = req.query;

  let sqlQueryForLogin = `SELECT * FROM studentDetails WHERE email='${email}' AND password='${password}'`;

  db.query(sqlQueryForLogin, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

// Get Request For Fetch Details Plus Sort Search Pagination & Exclude
app.get(`/studentDetails`, (req, res) => {
  const { sort, order, limit, page, q, id_ne } = req.query;
  offset_value = (page - 1) * limit;
  // console.log({ id_ne });
  let sqlQuery = `SELECT * 
  FROM studentDetails 
  WHERE id != ${id_ne}
  ORDER BY ${sort} ${order} 
  LIMIT ${limit} 
  OFFSET ${offset_value}`;
  if (q.length > 0) {
    sqlQuery = `SELECT * FROM studentDetails WHERE (name LIKE '%${q}%' OR email LIKE '%${q}%' OR phone LIKE '%${q}%' OR city LIKE '%${q}%' OR state LIKE '%${q}%' OR country LIKE '%${q}%' OR gender LIKE '%${q}%' OR courses LIKE '%${q}%') AND id != ${id_ne} ORDER BY ${sort} ${order} LIMIT ${limit} OFFSET ${offset_value}`;
  }

  let sqlQueryForSearch = `SELECT * FROM studentDetails WHERE id != ${id_ne}`;
  if (q.length > 0) {
    sqlQueryForSearch = `SELECT * FROM studentDetails WHERE (name LIKE '%${q}%' OR email LIKE '%${q}%' OR phone LIKE '%${q}%' OR city LIKE '%${q}%' OR state LIKE '%${q}%' OR country LIKE '%${q}%' OR gender LIKE '%${q}%' OR courses LIKE '%${q}%') AND id != ${id_ne}`;
  }

  let mainResult = [];
  db.query(sqlQuery, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      mainResult.push({ pageData: result });
      db.query(sqlQueryForSearch, (err, result) => {
        if (err) {
          console.log(err);
        } else {
          mainResult.push({ totalLength: result.length });
          res.send(mainResult);
        }
      });
    }
  });
});

// Add Data to data base
app.post("/studentDetails", (req, res) => {
  const {
    name,
    email,
    password,
    gender,
    phone,
    city,
    state,
    country,
    courses,
  } = req.body;

  db.query(
    `INSERT INTO 
  studentDetails (
    name,email,password,gender,phone,city,state,country,courses)
  VALUES ('${name}','${email}','${password}','${gender}','${phone}','${city}','${state}','${country}','${courses}')`,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Values Instered");
      }
    }
  );
});

// Delete data from database
app.delete("/studentDetails/:id", (req, res) => {
  const id = req.params.id;

  db.query(`DELETE FROM studentDetails WHERE id = ${id}`, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

// Update data
app.put("/studentDetails/:id", (req, res) => {
  const id = req.params.id;
  const { name, email, gender, phone, city, state, country, courses } =
    req.body;
  db.query(
    `UPDATE studentDetails
    SET name = '${name}', email = '${email}', gender = '${gender}', phone = '${phone}', city = '${city}', state = '${state}', country = '${country}', courses = '${courses}'
    WHERE id = '${id}'`,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Updated");
      }
    }
  );
});

const port = process.env.PORT || 8000;
app.listen(port, () => console.log("App is running on ", port));
